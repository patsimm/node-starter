module.exports = {
  printWidth: 100,
  proseWrap: 'never',
  semi: false,
  singleQuote: true
}
