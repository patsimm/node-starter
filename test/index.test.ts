import { init } from '../src'

describe('index.js', () => {
  describe('init', () => {
    it('should return true', () => {
      expect(init()).toStrictEqual(true)
    })
  })
})
